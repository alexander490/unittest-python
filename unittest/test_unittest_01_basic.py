import unittest
class TestBasicString(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(),'FOO')

    def test_isupper(self):
        self.assertFalse('foo'.isupper())
        self.assertTrue('FOO'.isupper())

    def test_split(self):
        string_test = 'hola mundo'
        self.assertEqual(string_test.split(), ['holax', 'mundoz'])

if __name__ == '__main__':
    unittest.main()
